package com.piaoniu.open.api.domain.activity.request;

import com.piaoniu.open.api.domain.BaseRequest;
import com.piaoniu.open.api.domain.activity.response.QueryActivitiesResponse;
import lombok.Data;
import lombok.ToString;

/**
 * @author daniel
 */
@Data
@ToString
public class QureyActivitiesRequest implements BaseRequest<QueryActivitiesResponse> {

    public static int MAX_PAGE_SIZE = 100;

    private int pageIndex = 1;

    private int pageSize = MAX_PAGE_SIZE;

    private int cityId;

    @Override
    public Class getResponseType() {
        return QueryActivitiesResponse.class;
    }

    @Override
    public String getUrl() {
        return "/openapi/v2/activities";
    }

    @Override
    public METHOD getMethod() {
        return METHOD.GET;
    }
}
