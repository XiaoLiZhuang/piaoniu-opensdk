package com.piaoniu.open.api.domain.activity.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author code4crafter@gmail.com
 *         Date: 2017/7/10
 *         Time: 下午6:00
 */
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Data
public class DeliverTypeDTO {

	private int type;
}
