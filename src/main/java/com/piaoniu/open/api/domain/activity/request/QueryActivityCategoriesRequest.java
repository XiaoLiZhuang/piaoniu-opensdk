package com.piaoniu.open.api.domain.activity.request;

import com.piaoniu.open.api.domain.BaseRequest;
import com.piaoniu.open.api.domain.activity.response.QueryActivityCategoriesResponse;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: daniel
 * @creed: keep it simple and stupid !
 * @Time: 2019/6/17 10:52 AM
 */
public class QueryActivityCategoriesRequest implements BaseRequest<QueryActivityCategoriesResponse> {
    @Override
    public Class getResponseType() {
        return QueryActivityCategoriesResponse.class;
    }

    @Override
    public String getUrl() {
        return "/openapi/v2/activityCategories/";
    }

    @Override
    public METHOD getMethod() {
        return METHOD.GET;
    }
}
