package com.piaoniu.open.api.domain.order.request;

import com.piaoniu.open.api.domain.BaseRequest;
import com.piaoniu.open.api.domain.order.dto.CreateOrderResponseDTO;
import com.piaoniu.open.api.domain.order.response.CreateOrderResponse;
import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;

/**
 * Created by slj on 16/12/26.
 */
@Data
@ToString
public class CreateOrderRequest implements BaseRequest<CreateOrderResponse> {

    private String orderId;

    private int ticketGroupId;

    /**
     * 暂时只有京东用拿来查ticketGroup
     */
    private int ticketCategoryId;

    private BigDecimal amount;

    private int count;

    private int deliverType;

    private String receiverName;

    private String phone;

    private String address;

    private String district;

    private String idCards;

    private BigDecimal postage;

    @Override
    public Class getResponseType() {
        return CreateOrderResponse.class;
    }

    @Override
    public String getUrl() {
        return "/openapi/v2/order/makeOrder/";
    }

    @Override
    public METHOD getMethod() {
        return METHOD.POST;
    }
}
