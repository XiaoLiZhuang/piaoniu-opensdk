package com.piaoniu.open.api.domain.activity.response;

import com.alibaba.fastjson.TypeReference;
import com.piaoniu.open.api.domain.BaseResponse;
import com.piaoniu.open.api.domain.activity.dto.TicketGroupDTO;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: daniel
 * @creed: keep it simple and stupid !
 * @Time: 2019/6/17 11:05 AM
 */
public class QueryStockAndPriceResponse extends BaseResponse<List<TicketGroupDTO>> {
    @Override
    public TypeReference getContentTypeReference() {
        return new TypeReference<List<TicketGroupDTO>>() {
        };
    }
}
