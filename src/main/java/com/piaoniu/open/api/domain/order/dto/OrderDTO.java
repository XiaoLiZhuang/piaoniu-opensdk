package com.piaoniu.open.api.domain.order.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by slj on 17/1/2.
 */
@Data
public class OrderDTO {

    private String orderId;

    private int pnOrderId;

    private int status;

    private BigDecimal totalAmount;

    private int ticketGroupId;

    private int count;

    private int deliverType;

    private String receiverName;

    private String phone;

    private String address;

    private OnSiteGetInfoDTO onSiteGetInfo;

    private List<TicketReceiptDTO> ticketReceipts;

    private ExpressInfoDTO expressInfo;

    private String deliverTimeNotice;

}
