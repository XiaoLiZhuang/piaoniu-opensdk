package com.piaoniu.open.api.domain.order.dto;

import lombok.Data;

/**
 * Created by evan on 2018/4/24.
 */
@Data
public class TicketReceiptDTO {
    String serialNumber;
    int status;
}
