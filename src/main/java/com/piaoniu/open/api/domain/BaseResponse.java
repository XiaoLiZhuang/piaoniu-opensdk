package com.piaoniu.open.api.domain;

import com.alibaba.fastjson.TypeReference;
import lombok.Data;
import lombok.ToString;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: daniel
 * @creed: keep it simple and stupid !
 * @Time: 2019/5/29 11:14 PM
 */
@Data
@ToString
public abstract class BaseResponse<T> {
    public static final String DEFAULT_ERROR_CODE = "-1";
    public static final String DEFAULT_ERROR_HINT = "系统错误";

    public static BaseResponse<Null> TIMEOUT_ERROR_RESPONSE;

    static {
        TIMEOUT_ERROR_RESPONSE = new BaseResponse<Null>() {
            @Override
            public TypeReference getContentTypeReference() {
                return new TypeReference<Null>() {
                };
            }
        };
        TIMEOUT_ERROR_RESPONSE.setData(Null.NULL);
        TIMEOUT_ERROR_RESPONSE.setErrorCode("408");
        TIMEOUT_ERROR_RESPONSE.setErrorHint("网络错误");
    }
    private String errorCode;
    private String errorHint;
    private T data;
    private boolean success;


    public BaseResponse fail(String errorCode, String errorHint) {
        this.setErrorCode(errorCode);
        this.setErrorHint(errorHint);
        this.setSuccess(false);
        return this;
    }

    public BaseResponse fail(String errorHint) {
        return fail(DEFAULT_ERROR_CODE, errorHint);
    }

    public BaseResponse success(T data) {
        this.setData(data);
        this.setSuccess(true);
        return this;
    }

    public abstract TypeReference getContentTypeReference();

}
