package com.piaoniu.open.api.domain.district.request;

import com.piaoniu.open.api.domain.BaseRequest;
import com.piaoniu.open.api.domain.district.response.QueryDistrictsResponse;
import com.piaoniu.open.api.domain.venue.response.QueryVenueResponse;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: daniel
 * @creed: keep it simple and stupid !
 * @Time: 2019/6/17 10:49 AM
 */
@Data
public class QueryDistrictsRequest implements BaseRequest<QueryDistrictsResponse> {

    private String parentCode;

    @Override
    public Class getResponseType() {
        return QueryDistrictsResponse.class;
    }

    @Override
    public String getUrl() {
        return "/openapi/v2/cities/district";
    }

    @Override
    public METHOD getMethod() {
        return METHOD.GET;
    }
}
