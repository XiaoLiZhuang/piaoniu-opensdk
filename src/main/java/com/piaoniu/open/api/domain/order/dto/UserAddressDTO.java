package com.piaoniu.open.api.domain.order.dto;

import lombok.Data;

/**
 * Created by slj on 16/12/29.
 */
@Data
public class UserAddressDTO {


    private int id;
    private int userId;  // 用户ID
    private String name; // 收件人姓名user
    private String phone; // 收件人电话
    private int addressType;
    private String address;
    private String idCardNumber;
    private int status;
}
