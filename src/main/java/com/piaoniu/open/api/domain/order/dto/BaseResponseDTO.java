package com.piaoniu.open.api.domain.order.dto;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: daniel
 * @creed: keep it simple and stupid !
 * @Time: 2019/6/14 10:26 AM
 */
@Data
public class BaseResponseDTO {
    boolean success;
    String errorCode;
    String errorHint;
}
