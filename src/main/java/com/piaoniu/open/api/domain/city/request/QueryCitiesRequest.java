package com.piaoniu.open.api.domain.city.request;

import com.piaoniu.open.api.domain.BaseRequest;
import com.piaoniu.open.api.domain.city.response.QueryCitiesResponse;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: daniel
 * @creed: keep it simple and stupid !
 * @Time: 2019/6/14 3:59 PM
 */
@Data
public class QueryCitiesRequest implements BaseRequest<QueryCitiesResponse> {
    @Override
    public Class getResponseType() {
        return QueryCitiesResponse.class;
    }

    @Override
    public String getUrl() {
        return "/openapi/v2/cities/";
    }

    @Override
    public METHOD getMethod() {
        return METHOD.GET;
    }
}
