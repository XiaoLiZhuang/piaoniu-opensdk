package com.piaoniu.open.api.domain.activity.dto;

import com.piaoniu.open.api.domain.activity.dto.TicketGroupDTO;
import lombok.Data;

import java.math.BigDecimal;

/**
 * Created by slj on 16/12/29.
 */
@Data
public class TicketCategoryDTO {

    private int id;

    private BigDecimal originPrice;

    private String specification;

    private String description;

    private int activityEventId;

    private int status;

    private TicketGroupDTO ticketGroup;

}
