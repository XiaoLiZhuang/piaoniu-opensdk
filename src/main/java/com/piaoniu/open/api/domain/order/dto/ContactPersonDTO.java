package com.piaoniu.open.api.domain.order.dto;

import lombok.Data;

import java.util.List;

/**
 * @author code4crafter@gmail.com
 *         Date: 2017/7/13
 *         Time: 下午12:01
 */
@Data
public class ContactPersonDTO {

	private String name;

	private List<String> phones;
}
