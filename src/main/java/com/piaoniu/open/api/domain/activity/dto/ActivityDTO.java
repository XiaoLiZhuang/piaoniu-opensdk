package com.piaoniu.open.api.domain.activity.dto;

import com.piaoniu.open.api.domain.venue.dto.VenueDTO;
import lombok.Data;

import java.util.List;

/**
 * Created by slj on 16/12/22.
 */
@Data
public class ActivityDTO {

    private int id;

    private int status;

    private String name;

    /**
     * shortname 不敢改成驼峰。。
     */
    private String shortName;

    private String poster;

    private int venueId;

    private int star;

    private String areaImage;

    private VenueDTO venue;

    private int categoryId;

    private int subCategoryId;

    private List<ActivityDetailDTO> details;

    private List<ActivityEventDTO> activityEvents;

    private boolean needIdCard;

    private Integer idCardUseTime;

    private int weight;

}
