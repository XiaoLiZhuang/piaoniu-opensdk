package com.piaoniu.open.api.domain.district.response;

import com.alibaba.fastjson.TypeReference;
import com.piaoniu.open.api.domain.BaseResponse;
import com.piaoniu.open.api.domain.district.dto.DistrictDTO;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: daniel
 * @creed: keep it simple and stupid !
 * @Time: 2019/6/17 10:49 AM
 */
public class QueryDistrictsResponse extends BaseResponse<List<DistrictDTO>> {
    @Override
    public TypeReference getContentTypeReference() {
        return new TypeReference<List<DistrictDTO>>() {
        };

    }
}
