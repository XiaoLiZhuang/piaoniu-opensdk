package com.piaoniu.open.api.domain.activity.response;

import com.alibaba.fastjson.TypeReference;
import com.piaoniu.open.api.domain.BaseResponse;
import com.piaoniu.open.api.domain.activity.dto.ActivityCategoryDTO;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: daniel
 * @creed: keep it simple and stupid !
 * @Time: 2019/6/17 10:53 AM
 */
public class QueryActivityCategoriesResponse extends BaseResponse<List<ActivityCategoryDTO>> {
    @Override
    public TypeReference getContentTypeReference() {
        return new TypeReference<List<ActivityCategoryDTO>>() {

        };
    }
}
