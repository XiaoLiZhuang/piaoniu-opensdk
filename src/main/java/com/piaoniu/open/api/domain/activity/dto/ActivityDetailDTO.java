package com.piaoniu.open.api.domain.activity.dto;

import lombok.Data;

/**
 * Created by slj on 16/12/30.
 */
@Data
public class ActivityDetailDTO {

    private int type;

    private String detailDesc;
}
