package com.piaoniu.open.api.domain.order.request;

import com.piaoniu.open.api.domain.BaseRequest;
import com.piaoniu.open.api.domain.order.response.QueryOrderResponse;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: daniel
 * @creed: keep it simple and stupid !
 * @Time: 2019/6/17 12:26 PM
 */
@Data
public class QueryOrderRequest implements BaseRequest<QueryOrderResponse> {
    private String orderId;

    @Override
    public Class getResponseType() {
        return QueryOrderResponse.class;
    }

    @Override
    public String getUrl() {
        return "/openapi/v2/order/detail/";
    }

    @Override
    public METHOD getMethod() {
        return METHOD.GET;
    }
}
