package com.piaoniu.open.api.domain.district.dto;

import lombok.Data;

/**
 * Created by slj on 16/12/29.
 */
@Data
public class DistrictDTO {

    private int id;

    private String code;

    private String name;

    private String parentCode;

    private int level;

    private int cityId;
}
