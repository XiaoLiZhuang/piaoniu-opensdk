package com.piaoniu.open.api.domain.order.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author code4crafter@gmail.com
 *         Date: 17/3/16
 *         Time: 上午11:47
 */
@Data
@NoArgsConstructor
public class CreateOrderResponseDTO extends BaseResponseDTO {
	private Integer orderId;

}
