package com.piaoniu.open.api.domain.order.request;

import com.piaoniu.open.api.domain.BaseRequest;
import com.piaoniu.open.api.domain.order.dto.PayOrderResponseDTO;
import com.piaoniu.open.api.domain.order.response.PayOrderResponse;
import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;

/**
 * Created by slj on 16/12/26.
 */
@Data
@ToString
public class PayOrderRequest implements BaseRequest<PayOrderResponse> {

    /**
     * 外部订单号
     */
    private String orderId;

    private BigDecimal amount;


    @Override
    public Class getResponseType() {
        return PayOrderResponse.class;
    }

    @Override
    public String getUrl() {
        return "/openapi/v2/order/pay/";
    }

    @Override
    public METHOD getMethod() {
        return METHOD.POST;
    }
}
