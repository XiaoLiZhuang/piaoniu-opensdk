package com.piaoniu.open.api.domain.order.response;

import com.alibaba.fastjson.TypeReference;
import com.piaoniu.open.api.domain.BaseResponse;
import com.piaoniu.open.api.domain.order.dto.OrderDTO;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: daniel
 * @creed: keep it simple and stupid !
 * @Time: 2019/6/17 12:26 PM
 */
public class QueryOrderResponse extends BaseResponse<OrderDTO> {
    @Override
    public TypeReference getContentTypeReference() {
        return new TypeReference<OrderDTO>() {

        };
    }
}
