package com.piaoniu.open.api.domain;

import java.io.Serializable;

public final class Null implements Serializable {
    private static final long serialVersionUID = -1L;
    public static final Null NULL = new Null();
    private Null(){}
}
