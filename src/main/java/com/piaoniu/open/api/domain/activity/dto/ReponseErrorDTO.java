package com.piaoniu.open.api.domain.activity.dto;

import lombok.Data;

/**
 * @author code4crafter@gmail.com
 *         Date: 17/3/16
 *         Time: 上午11:14
 */
@Data
public class ReponseErrorDTO {

	private String code;

	private String message;
}
