package com.piaoniu.open.api.domain.activity.request;

import com.piaoniu.open.api.domain.BaseRequest;
import com.piaoniu.open.api.domain.activity.response.QueryActivityDetailResponse;
import lombok.Data;

/**
 * @author daniel
 */
@Data
public class QueryActivityDetailRequest implements BaseRequest<QueryActivityDetailResponse> {

    private int activityId;

    @Override
    public Class getResponseType() {
        return QueryActivityDetailResponse.class;
    }

    @Override
    public String getUrl() {
        return "/openapi/v2/activities/detail/";
    }

    @Override
    public METHOD getMethod() {
        return METHOD.GET;
    }
}
