package com.piaoniu.open.api.domain.activity.response;

import com.alibaba.fastjson.TypeReference;
import com.piaoniu.open.api.domain.BaseResponse;
import com.piaoniu.open.api.domain.activity.dto.ActivityDTO;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: daniel
 * @creed: keep it simple and stupid !
 * @Time: 2019/6/17 11:02 AM
 */
public class QueryActivityDetailResponse extends BaseResponse<ActivityDTO> {

    @Override
    public TypeReference getContentTypeReference() {
        return new TypeReference<ActivityDTO>() {

        };
    }
}
