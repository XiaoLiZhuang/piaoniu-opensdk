package com.piaoniu.open.api.domain.venue.request;

import com.piaoniu.open.api.domain.BaseRequest;
import com.piaoniu.open.api.domain.venue.response.QueryVenueResponse;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: daniel
 * @creed: keep it simple and stupid !
 * @Time: 2019/6/17 10:37 AM
 */
@Data
public class QueryVenueRequest implements BaseRequest<QueryVenueResponse> {

    private int venueId;

    @Override
    public Class getResponseType() {
        return QueryVenueResponse.class;
    }

    @Override
    public String getUrl() {
        return "/openapi/v2/venue/";
    }

    @Override
    public METHOD getMethod() {
        return METHOD.GET;
    }
}
