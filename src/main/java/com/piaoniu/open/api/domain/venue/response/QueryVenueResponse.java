package com.piaoniu.open.api.domain.venue.response;

import com.alibaba.fastjson.TypeReference;
import com.piaoniu.open.api.domain.BaseResponse;
import com.piaoniu.open.api.domain.venue.dto.VenueDTO;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: daniel
 * @creed: keep it simple and stupid !
 * @Time: 2019/6/17 10:37 AM
 */
public class QueryVenueResponse extends BaseResponse<VenueDTO> {
    @Override
    public TypeReference getContentTypeReference() {
        return new TypeReference<VenueDTO>() {
        };
    }
}
