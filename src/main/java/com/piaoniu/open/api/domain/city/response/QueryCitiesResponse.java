package com.piaoniu.open.api.domain.city.response;

import com.alibaba.fastjson.TypeReference;
import com.piaoniu.open.api.domain.BaseResponse;
import com.piaoniu.open.api.domain.PageModel;
import com.piaoniu.open.api.domain.city.dto.CityDTO;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: daniel
 * @creed: keep it simple and stupid !
 * @Time: 2019/6/17 10:20 AM
 */
public class QueryCitiesResponse extends BaseResponse<List<CityDTO>> {
    @Override
    public TypeReference getContentTypeReference() {
        return new TypeReference<List<CityDTO>>() {
        };
    }

}
