package com.piaoniu.open.api.domain.activity.request;

import com.piaoniu.open.api.domain.BaseRequest;
import com.piaoniu.open.api.domain.activity.response.QueryActivityDetailResponse;
import com.piaoniu.open.api.domain.activity.response.QueryStockAndPriceResponse;
import lombok.Data;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: daniel
 * @creed: keep it simple and stupid !
 * @Time: 2019/6/17 11:04 AM
 */
@Data
public class QueryStockAndPriceRequest implements BaseRequest<QueryStockAndPriceResponse> {
    private List<Integer> ids;

    @Override
    public Class getResponseType() {
        return QueryStockAndPriceResponse.class;
    }

    @Override
    public String getUrl() {
        return "/openapi/v2/tickets/batchDetails/";
    }

    @Override
    public METHOD getMethod() {
        return METHOD.GET;
    }
}
