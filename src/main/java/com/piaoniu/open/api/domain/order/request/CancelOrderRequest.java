package com.piaoniu.open.api.domain.order.request;

import com.piaoniu.open.api.domain.BaseRequest;
import com.piaoniu.open.api.domain.order.response.CancelOrderResponse;
import lombok.Data;
import lombok.ToString;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: daniel
 * @creed: keep it simple and stupid !
 * @Time: 2019/6/17 11:21 AM
 */

@Data
@ToString
public class CancelOrderRequest implements BaseRequest<CancelOrderResponse> {

    private String orderId;

    @Override
    public Class getResponseType() {
        return CancelOrderResponse.class;
    }

    @Override
    public String getUrl() {
        return "/openapi/v2/order/cancel/";
    }

    @Override
    public METHOD getMethod() {
        return METHOD.POST;
    }
}
