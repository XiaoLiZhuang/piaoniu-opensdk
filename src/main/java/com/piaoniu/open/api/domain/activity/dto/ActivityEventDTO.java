package com.piaoniu.open.api.domain.activity.dto;

import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * Created by slj on 16/12/29.
 */
@Data
public class ActivityEventDTO {

    private int id;

    private String specification;

    private Date start;

    private Date end;

    private int status;

    private List<TicketCategoryDTO> ticketCategories;

    private Date defaultEnd;
}
