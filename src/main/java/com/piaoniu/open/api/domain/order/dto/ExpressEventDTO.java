package com.piaoniu.open.api.domain.order.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author code4crafter@gmail.com Date: 2017/11/1 Time: 下午5:22
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ExpressEventDTO {

    private String description;

    private Date time;

}
