package com.piaoniu.open.api.domain.activity.dto;

import lombok.Data;

/**
 * Created by slj on 17/1/16.
 */
@Data
public class ActivityCategoryDTO {

    private int id;

    private String name;

    private String enName;
}
