package com.piaoniu.open.api.domain.order.response;

import com.alibaba.fastjson.TypeReference;
import com.piaoniu.open.api.domain.BaseResponse;
import com.piaoniu.open.api.domain.order.dto.CancelOrderResponseDTO;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: daniel
 * @creed: keep it simple and stupid !
 * @Time: 2019/6/17 11:15 AM
 */
public class CancelOrderResponse extends BaseResponse<CancelOrderResponse> {
    @Override
    public TypeReference getContentTypeReference() {
        return new TypeReference<CancelOrderResponseDTO>() {
        };
    }
}
