package com.piaoniu.open.api.domain;

public interface BaseRequest<T extends BaseResponse> {

    enum METHOD {
        GET,
        POST;
    }

    Class getResponseType();

    String getUrl();

    //"GET" or "POST"
    METHOD getMethod();
}
