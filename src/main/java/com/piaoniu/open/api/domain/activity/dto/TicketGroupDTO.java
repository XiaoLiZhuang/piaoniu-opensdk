package com.piaoniu.open.api.domain.activity.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by slj on 16/12/29.
 */
@Data
@NoArgsConstructor
public class TicketGroupDTO {

    private int ticketCategoryId;

    private int ticketGroupId;

    private BigDecimal salePrice;

    private BigDecimal basePrice;

    private int isPremium;

    private BigDecimal premiumPrice;

    private int selling;

    private List<DeliverTypeDTO> deliverTypes;

    private List<BigDecimal> priceList;

    private Integer promiseDeliverDay;
}
