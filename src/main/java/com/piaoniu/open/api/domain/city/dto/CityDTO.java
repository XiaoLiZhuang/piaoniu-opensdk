package com.piaoniu.open.api.domain.city.dto;

import lombok.Data;

/**
 * Created by slj on 16/12/29.
 */
@Data
public class CityDTO  {

    private int cityId;
    private String cityName;
    private String cityEnName;
    private String cityAbbrCode;

}
