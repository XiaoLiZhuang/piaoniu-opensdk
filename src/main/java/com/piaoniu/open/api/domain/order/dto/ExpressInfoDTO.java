package com.piaoniu.open.api.domain.order.dto;

import lombok.Data;

import java.util.List;

/**
 * @author code4crafter@gmail.com
 *         Date: 2017/7/12
 *         Time: 下午12:01
 */
@Data
public class ExpressInfoDTO {

	private String company;

	private String expressNo;

	private List<ExpressEventDTO> expressEvents;
}
