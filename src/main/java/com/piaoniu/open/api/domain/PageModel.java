package com.piaoniu.open.api.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
public class PageModel<T> implements Serializable {

    private static final long serialVersionUID = -8871167960445103162L;

    private static final int MAX_PAGE_SIZE = 100;

    long totalNum;
    int pageIndex;
    int pageSize;
    List<T> data = new ArrayList();

    public PageModel() {
    }


    public boolean isHasMore() {
        return pageIndex * pageSize < totalNum;
    }

}
