package com.piaoniu.open.api.domain.order.dto;

import com.piaoniu.open.api.domain.order.dto.BaseResponseDTO;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author song
 * @Create 19-5-24 下午4:37
 */
@Data
@NoArgsConstructor
public class CancelOrderResponseDTO extends BaseResponseDTO {
    private String message;
}
