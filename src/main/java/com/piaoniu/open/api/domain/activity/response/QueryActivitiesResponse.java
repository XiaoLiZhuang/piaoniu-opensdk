package com.piaoniu.open.api.domain.activity.response;

import com.alibaba.fastjson.TypeReference;
import com.piaoniu.open.api.domain.BaseResponse;
import com.piaoniu.open.api.domain.PageModel;
import com.piaoniu.open.api.domain.activity.dto.ActivityDTO;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: daniel
 * @creed: keep it simple and stupid !
 * @Time: 2019/6/13 2:43 PM
 */
@Data
public class QueryActivitiesResponse extends BaseResponse<PageModel<ActivityDTO>> {

    @Override
    public TypeReference getContentTypeReference() {
        return new TypeReference<PageModel<ActivityDTO>>() {
        };
    }
}
