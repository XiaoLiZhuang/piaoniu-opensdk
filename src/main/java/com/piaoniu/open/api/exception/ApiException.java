package com.piaoniu.open.api.exception;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: daniel
 * @creed: keep it simple and stupid !
 * @Time: 2019/6/13 9:03 PM
 */
public class ApiException extends RuntimeException {
    public ApiException(String message) {
        super(message);
    }
}
