package com.piaoniu.open.api;


import com.piaoniu.open.api.domain.BaseRequest;
import com.piaoniu.open.api.domain.BaseResponse;
import com.piaoniu.open.utils.MapUtils;

import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * 需要考虑的问题：
 * 1.参数如何传入
 * User: daniel
 * Date: 2018-07-05
 * Time: 下午9:56
 */
public class PNApi extends AbstractApi {

    public PNApi(String baseUrl, String appId, String appKey) {
        super(baseUrl, appId, appKey);
    }

    //base
    private static final String CITIES_GET = "/openapi/v2/cities";

    private static final String VENUE_DETAIL = "/openapi/v2/venue";

    //item

    private static final String ACTIVITY_DETAIL = "/openapi/v2/activities/detail";

    private static final String PRICE_AND_STOCK = "/openapi/v2/tickets/batchDetails";

    //order
    private static final String MAKE_ORDER = "/openapi/v2/order/makeOrder";

    private static final String PAY_ORDER = "/openapi/v2/order/pay";

    private static final String ORDER_DETAIL = "/openapi/v2/order/detail";


    public <R extends BaseResponse> R execute(BaseRequest<R> t) {
        Map<String, String> map = MapUtils.objectToStringMap(t);
        formatAndRendeCommon(map);

        Class responseType = t.getResponseType();
        BaseRequest.METHOD method = t.getMethod();
        if (BaseRequest.METHOD.GET == method) {
            return (R) doGet(t.getUrl(), map, responseType);
        }
        return (R) doPost(t.getUrl(), map, responseType);

    }


}
