package com.piaoniu.open.http;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: daniel
 * @creed: keep it simple and stupid !
 * @Time: 2019/6/12 4:42 PM
 */
public class HttpClientFactory {
    private static volatile ConcurrentHashMap<Integer, PnHttpClient> pnHttpClientConcurrentHashMap = new ConcurrentHashMap<>();

    //todo check singleton
    public static PnHttpClient getSingletonWithProperties(HttpProperties httpProperties) {
        if(httpProperties==null){
            httpProperties = HttpProperties.DEFAULT;
        }

        int key = httpProperties.hashCode();
        if (null == pnHttpClientConcurrentHashMap.get(key)) {
            synchronized (PnHttpClient.class) {
                if (null == pnHttpClientConcurrentHashMap.get(key)) {
                    pnHttpClientConcurrentHashMap.put(key, new PnHttpClient(httpProperties));
                }
            }
        }
        return pnHttpClientConcurrentHashMap.get(key);
    }

}
