package com.piaoniu.open.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.function.Function;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: daniel
 * @creed: keep it simple and stupid !
 * @Time: 2019/6/12 11:52 AM
 */
@Slf4j
public class MapUtils {
    private static Map<Class, Function<Object, String>> stringConverters = new HashMap<>();

    static {
        regStringConverter();
    }

    //any custom toString converter
    //e.g.
    //SimpleDateFormat sdf = new SimpleDateFormat("");
    //stringConverters.put(Date.class, e -> sdf.format(e));
    private static void regStringConverter() {
        //fixme 这里可能为客户端提供的功能有点多，枚举漏了会出bug。后续优化
        stringConverters.put(ArrayList.class, e -> {
            List list = (List) e;
            return StringUtils.join(list, ",");
        });

        stringConverters.put(LinkedList.class, e -> {
            List list = (List) e;
            return StringUtils.join(list, ",");
        });

    }

    //获取所有的field getter
    //toString
    public static Map<String, String> objectToStringMap(Object object) {
        Map<Field, Method> fieldGetters = ReflectionUtils.getFieldGetters(object.getClass());

        Map<String, String> result = new HashMap<>();
        for (Map.Entry<Field, Method> fieldGetter : fieldGetters.entrySet()) {
            Method method = fieldGetter.getValue();
            Field field = fieldGetter.getKey();
            try {
                Object value = method.invoke(object, new Object[]{});
                Class<?> fieldClass = value.getClass();

                String stringValue;
                //基础类型
                if (fieldClass.isPrimitive()) {
                    stringValue = String.valueOf(value);
                } else {
                    if (stringConverters.containsKey(fieldClass)) {
                        //对象特殊处理
                        stringValue = stringConverters.get(fieldClass).apply(value);
                    } else {
                        //对象默认处理
                        stringValue = value.toString();
                    }
                }

                result.put(field.getName(), stringValue);

            } catch (Exception e) {
                log.warn("objectToStringMap failed : {}", ExceptionUtils.flatStackToString(e));
            }
        }
        return result;
    }
}
