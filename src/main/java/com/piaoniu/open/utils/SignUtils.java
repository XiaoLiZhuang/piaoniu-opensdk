package com.piaoniu.open.utils;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;

/**
 * @author code4crafter@gmail.com
 *         Date: 15/12/15
 *         Time: 下午12:14
 */
public class SignUtils {

	public static String getSign(Map<String, String> bizParams, String key) {
		return getSign(bizParams, key, true);
	}

	private static String getSign(Map<String, String> map, String key,boolean toLowerCase) {
		ArrayList list = new ArrayList();
		Iterator<Map.Entry<String,String>> size = map.entrySet().iterator();

		while(size.hasNext()) {
			Map.Entry<String,String> arrayToSort = size.next();
			if (arrayToSort.getValue() != null && arrayToSort.getValue() != "") {
				list.add((toLowerCase ? arrayToSort.getKey().toLowerCase() : arrayToSort.getKey()) + "=" + arrayToSort
						.getValue() + "&");
			}
		}

		int var6 = list.size();
		String[] var7 = (String[])list.toArray(new String[var6]);
		Arrays.sort(var7, String.CASE_INSENSITIVE_ORDER);
		StringBuilder sb = new StringBuilder();

		for(int result = 0; result < var6; ++result) {
			sb.append(var7[result]);
		}

		String var8 = sb.toString();
		var8 = var8 + "key=" + key;
		var8 = MD5.MD5Encode(var8).toUpperCase();
		return var8;
	}

}
