package com.piaoniu.open.api;

import com.alibaba.fastjson.JSONObject;
import com.piaoniu.open.api.domain.BaseResponse;
import com.piaoniu.open.api.domain.activity.request.QueryActivityDetailRequest;
import com.piaoniu.open.api.domain.activity.request.QueryStockAndPriceRequest;
import com.piaoniu.open.api.domain.activity.request.QureyActivitiesRequest;
import com.piaoniu.open.api.domain.activity.response.QueryActivitiesResponse;
import com.piaoniu.open.api.domain.activity.response.QueryActivityDetailResponse;
import com.piaoniu.open.api.domain.activity.response.QueryStockAndPriceResponse;
import com.piaoniu.open.api.domain.city.request.QueryCitiesRequest;
import com.piaoniu.open.api.domain.city.response.QueryCitiesResponse;
import com.piaoniu.open.api.domain.order.request.CancelOrderRequest;
import com.piaoniu.open.api.domain.district.request.QueryDistrictsRequest;
import com.piaoniu.open.api.domain.district.response.QueryDistrictsResponse;
import com.piaoniu.open.api.domain.order.request.CreateOrderRequest;
import com.piaoniu.open.api.domain.order.request.PayOrderRequest;
import com.piaoniu.open.api.domain.order.request.QueryOrderRequest;
import com.piaoniu.open.api.domain.order.response.CancelOrderResponse;
import com.piaoniu.open.api.domain.order.response.CreateOrderResponse;
import com.piaoniu.open.api.domain.order.response.PayOrderResponse;
import com.piaoniu.open.api.domain.order.response.QueryOrderResponse;
import com.piaoniu.open.api.domain.venue.request.QueryVenueRequest;
import com.piaoniu.open.api.domain.venue.response.QueryVenueResponse;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: daniel
 * @creed: keep it simple and stupid !
 * @Time: 2019/6/13 9:12 AM
 */
public class PNApiTest extends TestCase {
    PNApi pnApi;

    @Before
    public void init() {
        pnApi = new PNApi("http://openapi.beta.piaoniu.com", "10001", "12qwaszxasqw12");
    }

    @Test
    public void testGetPnVenueDetail() {
        init();
        QueryVenueRequest queryVenueRequest = new QueryVenueRequest();
        queryVenueRequest.setVenueId(9);
        QueryVenueResponse execute = pnApi.execute(queryVenueRequest);
        System.out.println(JSONObject.toJSONString(execute));
    }

    @Test
    public void testCities() {
        init();
        QueryCitiesRequest queryCitiesRequest = new QueryCitiesRequest();
        QueryCitiesResponse execute = pnApi.execute(queryCitiesRequest);
        System.out.println(JSONObject.toJSONString(execute));

    }

    @Test
    public void testActivities() {
        init();
        QureyActivitiesRequest qureyActivitiesRequest = new QureyActivitiesRequest();
        qureyActivitiesRequest.setCityId(1);
        qureyActivitiesRequest.setPageIndex(1);
        qureyActivitiesRequest.setPageSize(10);
        BaseResponse execute = pnApi.execute(qureyActivitiesRequest);
        System.out.println(JSONObject.toJSONString(execute));

    }

    @Test
    public void testGetActivityDetail() {
        init();
        QueryActivityDetailRequest queryActivityDetailRequest = new QueryActivityDetailRequest();
        queryActivityDetailRequest.setActivityId(112883);

        QueryActivityDetailResponse execute = pnApi.execute(queryActivityDetailRequest);
        System.out.println(JSONObject.toJSONString(execute));
    }

    @Test
    public void testTicketBatchDetails() {
        init();
        QueryStockAndPriceRequest queryStockAndPriceRequest = new QueryStockAndPriceRequest();

        List<Integer> ticketCategoryIds = new ArrayList<>();
        ticketCategoryIds.add(1541440);
        queryStockAndPriceRequest.setIds(ticketCategoryIds);

        QueryStockAndPriceResponse execute = pnApi.execute(queryStockAndPriceRequest);
        System.out.println(execute);
    }

    @Test
    public void testMakerOrder() {
        init();

        CreateOrderRequest makeOrderRequest = new CreateOrderRequest();
        makeOrderRequest.setOrderId("44444");
        makeOrderRequest.setCount(1);
        makeOrderRequest.setAmount(new BigDecimal("220"));
        makeOrderRequest.setTicketCategoryId(1541440);
        makeOrderRequest.setDeliverType(3);
        makeOrderRequest.setReceiverName("daniel");
        makeOrderRequest.setPhone("17621111111");
        makeOrderRequest.setPostage(new BigDecimal("0"));
        makeOrderRequest.setDistrict("");
        makeOrderRequest.setAddress("");
        CreateOrderResponse execute = pnApi.execute(makeOrderRequest);
        System.out.println(execute);
    }

    //    @Test
//    public void testMakerOrderDeliver() {
//        init();
//        MakeOrderRequest makeOrderRequest = new MakeOrderRequest();
//        makeOrderRequest.setOrderId("24444");
//        makeOrderRequest.setCount(1L);
//        makeOrderRequest.setAmount(new BigDecimal("210"));
//        makeOrderRequest.setTicketCategoryId(1541440);
//        makeOrderRequest.setDeliverType(3);
//        makeOrderRequest.setReceiverName("daniel");
//        makeOrderRequest.setPhone("17621111111");
//        makeOrderRequest.setPostage(new BigDecimal("0"));
//        makeOrderRequest.setDistrict("sadfasdfasdf");
//        makeOrderRequest.setAddress("asdfasdfsadf");
//        BaseResponse<MakeOrderResponse> makeOrderResponseBaseResponse = pnApi.makerOrder(makeOrderRequest);
//        System.out.println(makeOrderResponseBaseResponse);
//    }
//
    @Test
    public void testPayOrder() {
        init();
        PayOrderRequest payOrderRequest = new PayOrderRequest();
        payOrderRequest.setAmount(new BigDecimal("220"));
        payOrderRequest.setOrderId("44444");
        PayOrderResponse execute = pnApi.execute(payOrderRequest);
        System.out.println(execute);
    }

    @Test
    public void testGetOrderDetail() {
        init();
        QueryOrderRequest queryOrderRequest = new QueryOrderRequest();
        queryOrderRequest.setOrderId("3976546");
        QueryOrderResponse execute = pnApi.execute(queryOrderRequest);
        System.out.println(execute);

    }

    @Test
    public void testGetActivities() {
        init();
        QureyActivitiesRequest qureyActivitiesRequest = new QureyActivitiesRequest();
        qureyActivitiesRequest.setCityId(1);
        qureyActivitiesRequest.setPageIndex(1);
        qureyActivitiesRequest.setPageSize(10);
        QueryActivitiesResponse execute = pnApi.execute(qureyActivitiesRequest);
        assertEquals(execute.isSuccess(), true);
        System.out.println(JSONObject.toJSONString(execute.getData().getData()));
    }

    @Test
    public void testQueryDistricts() {
        init();
        QueryDistrictsRequest queryDistrictsRequest= new QueryDistrictsRequest();
        queryDistrictsRequest.setParentCode("110100");
        QueryDistrictsResponse execute = pnApi.execute(queryDistrictsRequest);
        assertEquals(execute.isSuccess(),true);
        System.out.println(JSONObject.toJSONString(execute.getData()));
    }


    @Test
    public void testOrderCancel() {

        init();
        CancelOrderRequest cancelOrderRequest = new CancelOrderRequest();
        cancelOrderRequest.setOrderId("44444");
        CancelOrderResponse execute = pnApi.execute(cancelOrderRequest);
        System.out.println(JSONObject.toJSONString(execute));
    }


}
